/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs = require('fs');
const path = require('path');
const filePath = path.resolve(__dirname, './data.json');


fs.readFile(filePath, 'utf-8', (error, data) => {
    if (error) {
        console.error(error)
    }
    else {
        //console.log(JSON.parse(data));
        let answer = JSON.parse(data);
        let problem1 = answer.employees.filter((elements) => {
            if (elements.id == 2 || elements.id == 13 || elements.id == 23) {
                //console.log(elements)
                return true;
            }
            else {
                return false;
            }
        });
        fs.writeFile('./probelm1.json', JSON.stringify(problem1), (err) => {
            if (err) {
                console.error(err);
            }
            else {
                console.log("success");
                fs.readFile(filePath, 'utf-8', (err, data) => {
                    if (err) {
                        console.error(err);
                    }
                    else {
                        let group = JSON.parse(data);
                        let groupData = group.employees.reduce((accumulater, current) => {
                            if (accumulater[current.company]) {
                                accumulater[current.company].push(current);
                            }
                            else{
                                accumulater[current.company]=[];
                            }
                        }, {})
                    }
                })
            }
        })
    }
})
